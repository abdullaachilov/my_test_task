FactoryBot.define do
  factory :product do
    name { Faker::Cannabis.brand }
    description { Faker::Lorem.words(number: rand(3..25)).join(' ') }
    price { rand(1.01..1499.99).round(2) }
  end
end
