# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  description :text
#  name        :string
#  price       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class ProductSerializer < ActiveModel::Serializer
  attributes :id, :title, :name, :description, :price

  has_many :tags

  # postman tests requires strings only
  # am I doing it wrong way?
  # I feel like there are must be some config for this
  def id
    object.id.to_s
  end

  def price
    object.price.to_s
  end

  def tags
    object.tags.map { |t| { id: t.id.to_s, title: t.name, name: t.name } }
  end

  def title
    object.name
  end
end
