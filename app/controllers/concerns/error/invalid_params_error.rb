module Error
  class InvalidParamsError < CustomError
    def initialize(_message)
      super(:wrong_params, 422, _message)
    end
  end
end
