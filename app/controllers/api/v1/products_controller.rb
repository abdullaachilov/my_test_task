module Api
  class V1::ProductsController < Api::V1Controller
    include Paginable

    before_action :set_pagination_params, only: %i[index]

    def index
      render json: Product.offset(@offset).limit(@limit)
    end

    def create
      product = Product.create! product_params_for_save
      render json: product
    rescue StandardError
      raise StandardError, product.errors.full_messages.join(' && ')
    end

    def update
      product = Product.find(params[:id])
      product.update_attributes! product_params_for_save
      render json: product
    rescue StandardError
      raise StandardError, product.errors.full_messages.join(' && ')
    end

    def destroy
      product = Product.find(params[:id])
      product.destroy
      render json: {}
    end

    private

    def product_params
      ActiveModelSerializers::Deserialization.jsonapi_parse(
        params,
        only: %i[name title description price tags]
      )
    end

    def product_params_for_save
      ProductMapperService.new(product_params).to_active_record_hash
    end

    def set_pagination_params
      @offset, @limit = pagination_offset_limit(pagination_params)
    end
  end
end
