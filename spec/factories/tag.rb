FactoryBot.define do
  factory :tag do
    name do
      [
        Faker::Cannabis.health_benefit,
        Faker::Cannabis.medical_use,
        Faker::Cannabis.category,
        Faker::Cannabis.type
      ].sample
    end
  end
end
