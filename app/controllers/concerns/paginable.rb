module Paginable
  include ActiveSupport::Concern

  DEFAULT_OFFSET = 0.freeze
  DEFAULT_LIMIT = 50.freeze
  ALLOWED_MAX_LIMIT = 150.freeze

  def pagination_offset_limit(pagination_params)
    @pagination_params = pagination_params
    [offset, limit]
  end

  def self.default_offset
    DEFAULT_OFFSET
  end

  def self.default_limit
    DEFAULT_LIMIT
  end

  private

  def offset
    return DEFAULT_OFFSET unless pagination_param_valid?(:offset)
    @pagination_params[:offset].to_i
  end

  def limit
    return DEFAULT_LIMIT unless pagination_param_valid?(:limit)
    limit = @pagination_params[:limit].to_i
    return ALLOWED_MAX_LIMIT if limit > ALLOWED_MAX_LIMIT
    limit
  end

  def pagination_param_valid?(pagination_param_name)
    @pagination_params.present? &&
      @pagination_params[pagination_param_name].present? &&
      @pagination_params[pagination_param_name].to_s.scan(/[^0-9]/).blank? &&
      @pagination_params[pagination_param_name].to_i >= 0
  rescue StandardError
    false
  end
end
