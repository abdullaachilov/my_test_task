# require 'rails_helper'
#
# RSpec.describe PaginationService do
#   describe 'create_offset_limit' do
#     context 'correct creation' do
#       pagination_params = { offset: 10, limit: 10 }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be 10' do
#         expect(offset).to eq 10
#       end
#
#       it 'limit shoudl be 10' do
#         expect(limit).to eq 10
#       end
#     end
#
#     context 'correct creation, but params are string' do
#       pagination_params = { offset: '15', limit: '150' }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be 15' do
#         expect(offset).to eq 15
#       end
#
#       it 'limit shoudl be 150' do
#         expect(limit).to eq 150
#       end
#     end
#
#     context 'correct creation, but limit more than allowed' do
#       pagination_params = { offset: '15', limit: 1_000_000 }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be 15' do
#         expect(offset).to eq 15
#       end
#
#       it 'limit shoudl be 150' do
#         expect(limit).to eq 150
#       end
#     end
#
#     context 'correct creation, but give only offset params' do
#       pagination_params = { offset: 25 }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be 15' do
#         expect(offset).to eq 25
#       end
#
#       it 'limit shoudl be 150' do
#         expect(limit).to eq PaginationService.default_limit
#       end
#     end
#
#     context 'correct creation, but give only limit params' do
#       pagination_params = { limit: 150 }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be 15' do
#         expect(offset).to eq PaginationService.defalt_offset
#       end
#
#       it 'limit shoudl be 150' do
#         expect(limit).to eq 150
#       end
#     end
#
#     context 'invalid creation - have no params' do
#       pagination_params = {}
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be defalt_offset' do
#         expect(offset).to eq PaginationService.defalt_offset
#       end
#
#       it 'limit shoudl be default_limit' do
#         expect(limit).to eq PaginationService.default_limit
#       end
#     end
#
#     context 'invalid creation broken params' do
#       pagination_params = { offset: 'lorem', limit: false }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be defalt_offset' do
#         expect(offset).to eq PaginationService.defalt_offset
#       end
#
#       it 'limit shoudl be default_limit' do
#         expect(limit).to eq PaginationService.default_limit
#       end
#     end
#
#     context 'invalid creation more broken params ' do
#       pagination_params = { offset: '12rem34', limit: false }
#       offset, limit =
#         PaginationService.new(pagination_params).pagination_offset_limit
#
#       it 'ofset shoudl be defalt_offset' do
#         expect(offset).to eq PaginationService.defalt_offset
#       end
#
#       it 'limit shoudl be default_limit' do
#         expect(limit).to eq PaginationService.default_limit
#       end
#     end
#   end
# end
