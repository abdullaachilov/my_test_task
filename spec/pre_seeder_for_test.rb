class PreSeederForTest
  # n == 0 - regular tests
  # n == 150_000 or more - perfomance tests (with tag: :perfomane only)
  def self.pre_seed_test_db(n)
    n.times { |i| FactoryBot.create(:product) }
  end
end
