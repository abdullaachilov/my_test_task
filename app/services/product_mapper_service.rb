class ProductMapperService
  def initialize(product_params)
    @product_params = product_params
  end

  def to_active_record_hash
    params = {}
    if @product_params[:name].present? || @product_params[:title].present?
      params[:name] =
        @product_params[:name] ||
          @product_params[:title].to_s.split(' ').first.to_s
    end
    if @product_params[:description].present?
      params[:description] = @product_params[:description]
    end
    params[:price] = @product_params[:price] if @product_params[:price].present?
    if @product_params[:tags].present?
      params[:tag_list] = @product_params[:tags]
    end
    params
  end
end
