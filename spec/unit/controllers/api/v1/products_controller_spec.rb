require 'rails_helper'

RSpec.describe Api::V1::ProductsController, type: :controller do
  describe '#index' do
    let!(:product_one) { create :product }
    let!(:product_two) { create :product }
    let!(:product_three) { create :product }
    let(:products) { [product_one, product_two, product_three] }

    context 'regular index request' do
      subject { get :index, params: {} }

      before(:each) do
        @parsed_response = JSON.parse(subject.body)
        @data = @parsed_response['data']
      end

      it 'returns http success' do
        expect(subject).to have_http_status(:success)
      end

      it 'has at least 3 object in return' do
        expect(@data.size).to be >= 3
      end

      it 'has random product as a responce' do
        expect(
          @data.find { |product| product['id'].to_s == product_one.id.to_s }
        ).should_not be_nil
      end

      it('should take reasonable time', perfomance: true) do
        expect { subject }.to perform_under(0.25).sample(5)
      end
    end

    context 'regular index request with 150 products and empty pagination params' do
      subject do
        products_params = []
        150.times do |i|
          products_params << FactoryBot.build(:product).attributes
        end
        Product.create products_params
        get :index, params: {}
      end

      before(:each) do
        @parsed_response = JSON.parse(subject.body)
        @data = @parsed_response['data']
      end

      it 'returns http success' do
        expect(subject).to have_http_status(:success)
      end

      it 'has to return 50 objects' do
        expect(@data.size).to eq 50
      end

      it('should take reasonable time', perfomance: true) do
        expect { subject }.to perform_under(0.25).sample(5)
      end
    end

    context 'regular index request with 150 products and correct pagination params' do
      params = { "utility": { "pagination": { "offset": 0, "limit": 25 } } }

      subject do
        products_params = []
        150.times do |i|
          products_params << FactoryBot.build(:product).attributes
        end
        Product.create products_params
        get :index, params: params
      end

      before(:each) do
        @parsed_response = JSON.parse(subject.body)
        @data = @parsed_response['data']
      end

      it 'returns http success' do
        expect(subject).to have_http_status(:success)
      end

      it 'has to return 25 objects' do
        expect(@data.size).to eq 25
      end

      it('should take reasonable time', perfomance: true) do
        expect { subject }.to perform_under(0.25).sample(5)
      end
    end

    context 'regular index request with offset > Product.all.size' do
      subject do
        get :index,
            params: {
              utility: { pagination: { offset: Product.all.size + 1 } }
            }
      end

      it 'returns http success' do
        expect(subject).to have_http_status(:success)
      end

      it 'should have 0 produts in response' do
        subject.body.should have_json_size(0).at_path 'data'
      end

      it('should take reasonable time', perfomance: true) do
        expect { subject }.to perform_under(0.25).sample(5)
      end
    end
  end

  describe '#create' do
    context 'correct one product creation' do
      params = {
        "data": {
          "type": 'undefined',
          "id": 'undefined',
          "attributes": {
            "name": 'Pepsi', "description": '24oz Bottle', "price": '1.98'
          }
        }
      }

      subject { post :create, params: params }

      # i feel like I should use include_json somehow,
      # but it iss sunday evening already, im sorry
      before(:each) do
        @parsed_response = JSON.parse(subject.body)
        @data = @parsed_response['data']
        @attributes = @parsed_response['data']['attributes']
        @relationships = @parsed_response['data']['relationships']
      end

      it 'returns http success' do
        expect(subject).to have_http_status(:success)
      end

      it 'returns created product - check id present' do
        subject.body.should have_json_path('data/id')
      end

      it 'returns created product - check name present' do
        subject.body.should have_json_path('data/attributes/name')
      end

      it 'returns created product - check description present' do
        subject.body.should have_json_path('data/attributes/description')
      end

      it 'returns created product - check price present' do
        subject.body.should have_json_path('data/attributes/price')
      end

      it 'returns created product - check name changed' do
        expect(@attributes['name']).to eq 'Pepsi'
      end

      it 'returns created product - check description changed' do
        expect(@attributes['description']).to eq '24oz Bottle'
      end

      it 'returns created product - check price changed' do
        expect(@attributes['price']).to eq '1.98'
      end
    end

    context 'attempt to create product with empty params' do
      params = {
        "data": {
          "type": 'undefined',
          "id": 'undefined',
          "attributes": { "name": '', "description": '', "price": '' }
        }
      }

      subject { post :create, params: params }

      it 'returns http status error' do
        expect(subject).to have_http_status(:error)
      end

      it 'errors list is not empty' do
        subject.body.should have_json_path('error')
      end
    end

    context 'attempt to create product with wrong price' do
      params = {
        "data": {
          "type": 'undefined',
          "id": 'undefined',
          "attributes": {
            "name": 'Correct name',
            "description": 'Correct description',
            "price": 'price is a text what a nonsence!'
          }
        }
      }

      subject { post :create, params: params }

      it 'returns http status error' do
        expect(subject).to have_http_status(:error)
      end

      it 'errors list is not empty' do
        subject.body.should have_json_path('error')
      end
    end

    context 'attempt to create product with shorn name' do
      params = {
        "data": {
          "type": 'undefined',
          "id": 'undefined',
          "attributes": {
            "name": 'E', "description": 'Correct description', "price": '4.20'
          }
        }
      }

      subject { post :create, params: params }

      it 'returns http status error' do
        expect(subject).to have_http_status(:error)
      end

      it 'errors list is not empty' do
        subject.body.should have_json_path('error')
      end
    end
  end

  describe '#update' do
    context 'correct product update' do
      let!(:product) { create :product, id: 1 }
      params = {
        "id": 1,
        "data": {
          "type": 'Product',
          "id": '1',
          "attributes": {
            "name": 'PepsiCola',
            "description": '12oz Bottle',
            "price": '1.98',
            "tags": %w[glass beverage]
          }
        }
      }

      subject do
        product
        patch :update, params: params
      end

      # i feel like I should use include_json somehow,
      # but it iss sunday evening already, im sorry
      before(:each) do
        @parsed_response = JSON.parse(subject.body)
        @data = @parsed_response['data']
        @attributes = @parsed_response['data']['attributes']
        @relationships = @parsed_response['data']['relationships']
      end

      it 'returns http success' do
        product
        expect(subject).to have_http_status(:success)
      end

      it 'returns created product - check id present' do
        subject.body.should have_json_path('data/id')
      end

      it 'returns created product - check name present' do
        subject.body.should have_json_path('data/attributes/name')
      end

      it 'returns created product - check description present' do
        subject.body.should have_json_path('data/attributes/description')
      end

      it 'returns created product - check price present' do
        subject.body.should have_json_path('data/attributes/price')
      end

      it 'returns created product - check tags present' do
        subject.body.should have_json_path('data/relationships/tags')
      end

      it 'returns created product - check name changed' do
        expect(@attributes['name']).to eq 'PepsiCola'
      end

      it 'returns created product - check description changed' do
        expect(@attributes['description']).to eq '12oz Bottle'
      end

      it 'returns created product - check price changed' do
        expect(@attributes['price']).to eq '1.98'
      end

      it 'returns created product - check amount of tags' do
        expect(@relationships['tags']['data'].size).to eq 2
      end
    end

    context 'product update with incorrect name' do
      let!(:product) { create :product, id: 1 }
      params = {
        "id": 1,
        "data": {
          "type": 'Product',
          "id": '1',
          "attributes": {
            "name": 'E', "description": '12oz Bottle', "price": '12.44'
          }
        }
      }

      subject do
        product
        patch :update, params: params
      end

      it 'returns http status error' do
        expect(subject).to have_http_status(:error)
      end

      it 'errors list is not empty' do
        subject.body.should have_json_path('error')
      end
    end

    context 'product update with incorrect price' do
      let!(:product) { create :product, id: 1 }
      params = {
        "id": 1,
        "data": {
          "type": 'Product',
          "id": '1',
          "attributes": {
            "name": 'Elevator wo',
            "description": '12oz Bottle',
            "price": 'fifty_cents'
          }
        }
      }

      subject do
        product
        patch :update, params: params
      end

      it 'returns http status error' do
        expect(subject).to have_http_status(:error)
      end

      it 'errors list is not empty' do
        subject.body.should have_json_path('error')
      end
    end

    context 'correct product update with duplicating tags' do
      let!(:product) { create :product, id: 1 }
      params = {
        "id": 1,
        "data": {
          "type": 'Product',
          "id": '1',
          "attributes": {
            "name": 'PepsiCola',
            "description": '12oz Bottle',
            "price": '1.98',
            "tags": %w[glass beverage glass]
          }
        }
      }

      subject do
        product
        patch :update, params: params
      end

      # i feel like I should use include_json somehow,
      # but it iss sunday evening already, im sorry
      before(:each) do
        @parsed_response = JSON.parse(subject.body)
        @data = @parsed_response['data']
        @relationships = @parsed_response['data']['relationships']
      end

      it 'returns http success' do
        product
        expect(subject).to have_http_status(:success)
      end

      it 'returns created product - check tags present' do
        subject.body.should have_json_path('data/relationships/tags')
      end

      it 'returns created product - check amount of tags' do
        expect(@relationships['tags']['data'].size).to eq 2
      end
    end
  end
end
