class ApplicationController < ActionController::API
  include Error::ErrorHandler

  before_action :downcase_params_keys

  private

  # use method from rails 6.1 by monkey-patching
  # backporting the method in initializers/deep_transform_keys_backport
  def downcase_params_keys
    params.deep_transform_keys!(&:downcase)
  end

  def pagination_params
    params.dig(:utility, :pagination)
  end
end
