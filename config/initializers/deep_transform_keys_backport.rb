# monkey patch for back-port
# deep_transform_keys, deep_transform_keys! methods from Rails 6.1
module DeepTransformKeys
  def deep_transform_keys!(&block)
    @parameters.deep_transform_keys!(&block)
    self
  end
end

ActionController::Parameters.include(DeepTransformKeys)
