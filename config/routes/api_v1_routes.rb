module ApiV1Routes
  def self.extended(router)
    router.instance_exec do
      namespace :api, defaults: { format: 'json' } do
        namespace :v1 do
          resources :products
        end
      end
    end # router.instance_exec
  end #def self.extended(router)
end #module ApiV1Routes
