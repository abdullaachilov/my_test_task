module Error
  module ErrorHandler
    def self.included(the_class)
      the_class.class_eval do
        rescue_from StandardError do |e|
          respond(:standard_error, 500, e.to_s)
        end

        rescue_from ActiveRecord::RecordNotFound do |e|
          respond(:record_not_found, 404, e.to_s)
        end
      end
    end

    private

    def respond(_error, _status, _message)
      render json: { error: _message }, status: _status
      # Im not sure it has to return false here
      # but in case to prevent possible(?) double rendering
      return false
    end
  end
end
