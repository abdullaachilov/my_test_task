ENV['RAILS_ENV'] ||= 'test'
ENV['SPRING_ENV'] ||= 'test'

require File.expand_path('../config/environment', __dir__)

if Rails.env.production?
  abort('The Rails environment is running in production mode!')
end
abort('The Rails environment is running in staging mode!') if Rails.env.staging?

require 'rspec/rails'
require 'database_cleaner'
require 'rspec-benchmark'
require 'factory_bot'
require 'pre_seeder_for_test'
require 'csv'

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  CSV.open("#{Rails.root}/memory_test_history.csv", 'a') do |csv|
    csv <<
      %w[________________ ________________ ________________ ________________]
  end
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!

  config.filter_rails_from_backtrace!

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.include RSpec::Benchmark::Matchers

  config.include FactoryBot::Syntax::Methods

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    #
    # spring service start
    # change PRODUCTS_COUNT
    # rspec
    # continue testing with
    # spring rspec
    # so, all preseeded data once loaded in spring parent process
    # and all spring forks just using it, while database cleaner
    # clean from new data, which was added in fork processes
    #
    # 0 - unit tests
    # 100_000 - large number for perfomance tests
    PRODUCTS_COUNT = 0

    if Product.all.size != PRODUCTS_COUNT
      DatabaseCleaner.clean_with(:truncation)
      puts '!!!!!! >>>>>>> PRESEED DB STARTED'
      PreSeederForTest.pre_seed_test_db PRODUCTS_COUNT
      puts '!!!!!! <<<<<<< PRESEED DB ENDED'
    end
  end

  config.around(:each) do |example|
    example.run
    CSV.open("#{Rails.root}/memory_test_history.csv", 'a') do |csv|
      csv <<
        [
          example.location,
          GetProcessMem.new.mb,
          Time.now.strftime('%d %b %y'),
          "PRODUCTS_COUNT: #{PRODUCTS_COUNT}"
        ]
    end
  end
end
