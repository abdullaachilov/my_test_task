require 'rails_helper'

RSpec.describe Product, type: :model do
  it 'is valid with valid attributes' do
    product = Product.new(name: 'Correct name', price: 12.34)
    expect(product).to be_valid
  end

  it 'is not valid with short name' do
    product = Product.new(name: 'E', price: 12.34)
    expect(product).to_not be_valid
  end

  it 'is not valid without a name' do
    product = Product.new(price: 12.34)
    expect(product).to_not be_valid
  end

  it 'is not valid with a price < 0' do
    product = Product.new(name: 'Correct name', price: -0.01)
    expect(product).to_not be_valid
  end

  it 'is not valid with a price == 0' do
    product = Product.new(name: 'Correct name', price: 0)
    expect(product).to_not be_valid
  end
end
