# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  description :text
#  name        :string
#  price       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Product < ApplicationRecord
  acts_as_taggable_on :tags
  MIN_NAME_LENGTH = 3.freeze

  validates :name, presence: true
  validates :name, length: { minimum: MIN_NAME_LENGTH }
  validates :price, numericality: { greater_than: 0 }
  validate :validate_tag

  def validate_tag
    tag_list.each do |tag|
      if tag.scan(/[^a-zA-Z0-9\s]/).present?
        errors.add(
          :tag_list,
          'Please use only symbols and/or nubmers in tag name'
        )
      end
    end
  end

  def self.min_name_length
    MIN_NAME_LENGTH
  end
end
